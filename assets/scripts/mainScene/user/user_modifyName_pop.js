/**
 * 修改昵称界面
 */
cc.Class({
    extends: cc.Component,

    properties: {
        edName:{
            default: null,
            type: cc.EditBox
        },
        labOldName:{
            default: null,
            type: cc.Label
        },

        _nickName:"",
    },

    // use this for initialization
    onLoad: function () {
        this.labOldName.string = User.getNickName();
        this._nickName = "";
    },

    onTextChanged: function(text, editbox, customEventData) {
        var txt = text;
        if(txt == "")
        {
            this._nickName = txt;
        }
        else
        {
            var re = txt.replace(/\s+/g,"");
            editbox.string = re;
            txt = re;
            this._nickName = txt;
        }
    },

    onTrue:function(){
         if(this.edName.string == undefined ||  this.edName.string == "")
         {
             ComponentsUtils.showTips("请先输入昵称");  
            return;
         }

         if(!Utils.isNiceName(this.edName.string))
         {
            ComponentsUtils.showTips("输入不正确");  
            return;
         }

         var nickName = this.edName.string.replace(/\s/g, "");
         cc.log("修改名字：" + nickName);    
         var recv = function recv(ret) { 
            ComponentsUtils.unblock();
            if(ret.Code == 0)
            {
                User.setNickName(nickName);
                window.Notification.emit("USER_informationrefresh","");
                window.Notification.emit("USER_useruirefresh","");
                ComponentsUtils.showTips("修改成功");       
                this.onClose();
            }
            else
            {
                ComponentsUtils.showTips(ret.Msg);
            }
        }.bind(this);
        var data = {
            Token:User.getLoginToken(),
            UserCode:User.getUserCode(),
            Nick:nickName,
        };
        CL.HTTP.sendRequestRet(DEFINE.HTTP_MESSAGE.CHANGENICK, data, recv.bind(this),"POST");  
        ComponentsUtils.block();
    },

    onClose:function(){
       this.node.getComponent("Page").backAndRemove();
    }

});
