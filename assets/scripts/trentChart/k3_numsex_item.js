/**
 * !#zh 快3走势图号码分布统计组件   (二同号)
 * @information 出现次数/平均遗漏/最大遗漏/最大连出
 */
cc.Class({
    extends: cc.Component,

    properties: {
        labDec:{
            default:null,
            type:cc.Label
        },
        labNums:{
            default:[],
            type:cc.Label
        },
        ndBg:{
            default:null,
            type:cc.Node
        },

        ndDecBg:{
            default:null,
            type:cc.Node
        },
        _data:null
    },

    // use this for initialization
    onLoad: function () {
        if(this._data != null ){
            this.ndBg.color = this._data.color;
            this.labDec.string = this._data.dec;
            this.labDec.node.color = this._data.decColor;
            this.ndDecBg.color = this._data.decBgColor;
            for(var i=0;i<this._data.nums.length;i++ )
            {
                this.labNums[i].string = this._data.nums[i];
                this.labNums[i].node.color = this._data.numColor;
            }
        }
    },

    /** 
    * 接收走势图号码统计信息
    * @method init
    * @param {Object} data
    */
    init:function(data){
        this._data = data;
    }
    
});
