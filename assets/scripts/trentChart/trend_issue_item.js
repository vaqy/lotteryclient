/**
 * !#zh 走势图冷热组件
 * @information 号码/和值,30期，50期，100期，遗漏
 */
cc.Class({
    extends: cc.Component,

    properties: {
        _data:null, 
        _itemId: null,   
    },

    // use this for initialization
    onLoad: function () {
        if(this._data != null)
        {
            this.node.color = this._data.color;
            var isuseStr = this._data.Isuse;
            this.node.getChildByName("labIssue").getComponent(cc.Label).string = isuseStr;
        }
    },

     /** 
    * 接收走势图冷热信息
    * @method init
    * @param {Object} data
    */
    init:function(data){
        this._data = data;
    },

    /** 
    * 刷新走势图冷热信息
    * @method updateData
    * @param {Object} data
    */
    updateData:function(data){
        if(data != null)
        {
            this.node.color = data.color;
            var isuseStr = data.Isuse;
            this.node.getChildByName("labIssue").getComponent(cc.Label).string = isuseStr;

        }    
    },

    /** 
    * 显示走势图冷热信息
    * @method show
    * @param {} 
    */
    onItemIndex: function(i){
        this._itemId = i;
    }

});
