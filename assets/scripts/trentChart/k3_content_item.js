/**
 * !#zh 快3走势图号码走势组件
 * @information 开奖号
 */
cc.Class({
    extends: cc.Component,

    properties: {
        //普通数字
        pfNumItem:{
            default:null,
            type:cc.Prefab
        },

        //带背景数字
        pfNumItem1:{
            default:null,
            type:cc.Prefab
        },

        _data:null
    },

    // use this for initialization
    onLoad: function () {
        var baseNums = ["12","13","14","15","16","23","24","25","26","34","35","36","45","46","56"];
        var numColor = new cc.Color(255,255,255);
        if(this._data != null)
        {
            this.node.color = this._data.color; //背景颜色
            for(var i=0;i<this._data.nums.length;i++)
            {
                if(this._data.nums[i] != 0)
                {
                    var numItem = cc.instantiate(this.pfNumItem);
                    var labNum = numItem.getChildByName("labNum");
                    labNum.getComponent(cc.Label).string = this._data.nums[i];
                    this.node.addChild(numItem);  
                }
                else
                {
                    var numItem1 = cc.instantiate(this.pfNumItem1);
                    var labNum = numItem1.getChildByName("labNum");
                    labNum.getComponent(cc.Label).string = baseNums[i];
                    labNum.color = numColor;//字体颜色
                    this.node.addChild(numItem1);  
                }
            }
        }
    },

    /** 
    * 接收走势图号码走势信息
    * @method init
    * @param {Object} data
    */
    init:function(data){
        this._data = data;
    }

});
